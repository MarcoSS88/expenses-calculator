// GLOBAL SCOPE //

var mainContainer = document.querySelector('.main-container')

// FINESTRA 1 //
var window1 = document.querySelector('.window1-container')
//sezione 1//
var sezione1_1 = document.querySelector('.sezione1_1')
var inputBudget = document.querySelector('#input-budget')
//sezione 2//
var divExpList = document.querySelector('.divExpList')
let ul = document.querySelector('.list')
//sezione 3//
var btnSettlePay = document.querySelector('.settlePayBtn')

// FINESTRA 2 //
var window2 = document.querySelector('.window2-container')
//sezione 1//
var caNcel = document.querySelector('.cancel')
let divNewExpense = document.querySelector('.divNewexpense')
let labelNewExpense = divNewExpense.children[0]
//sezione 2//
var inputToe = document.querySelector('#input-toe')
//sezione 3//
var btnCash = document.querySelector('.btn1')
var btnDebit = document.querySelector('.btn2')
var btnCredit = document.querySelector('.btn3')
//sezione 5//
var btnConfirm = document.querySelector('.confirmBtn')
let bigEditBtn = document.querySelector('.BigEditBtn')

let initialBudget = document.createElement('p')

let thisVeryListItem = null

//VARIABILI CREATE PER LA SOTTRAZZIONE

let totalExpenses = []
let sommaDaSottrarre

// VARIABILI CREATE PER SELEXIONARE IL TOP //

let btnCashIsSelected = false
let btnCreditIsSelected = false
let btnDebitIsSelected = false


// Con questa funzione passerò all'altra finestra attraverso il click del button //

var settlePay = function () {

  initialBudget.className = 'initialBudget'

  initialBudget.innerHTML = inputBudget.value

  sezione1_1.appendChild(initialBudget)

  bigEditBtn.style.display = 'none'

  inputBudget.style.display = 'none'
  window1.style.display = 'none'
  window2.style.display = 'block'

  return initialBudget.innerHTML

}

btnSettlePay.addEventListener('click', function () {
  settlePay()
})


// Con questa funz. potrò tornare alla window1 (per ora) //

var getBacktoW1 = function () {

  labelNewExpense.innerHTML = 'New expense'

  bigEditBtn.style.display = 'none'
  btnConfirm.style.display = 'block'


  window2.style.display = 'none'
  window1.style.display = 'block'
}

caNcel.addEventListener('click', function () {
  getBacktoW1()
})

// TASTO INVIO // 

inputBudgetActive = true

window.addEventListener('keydown', function (event) {

  if (event.keyCode === 13 && inputBudgetActive === false) {

    settlePay()
  } else if (event.keyCode === 13 && inputBudgetActive === true) {

    settlePay()
  }

})


// CON QUESTA FUNZIONE AGGIUNGO GLI ELEMENTI ALLA UNORDED LIST //

var addElemInUl = function () {

  var li = document.createElement('li')
  var textToe = document.createElement('p')
  var textCost = document.createElement('p')
  var divElListExpItem = document.createElement('div')
  
  var div2btns = document.createElement('div')
  var btnDelete = document.createElement('button')
  var btnEdit = document.createElement('button')

  let divtextToPayment = document.createElement('div')
  let textToPayment = document.createElement('p')

  li.className = 'listExpItem'
  divElListExpItem.className = 'divElem'
  textCost.className = 'pCost'
  textToe.className = 'pToe'
  
  div2btns.className = 'div2btns'
  btnEdit.className = 'btnEdit'
  btnDelete.className = 'btnDelete'

  divtextToPayment.className = 'divtextToPayment'
  
  textToPayment.className = 'textToPayment'
  textToPayment.innerHTML = ''

  textToe.innerHTML = inputToe.value
  textCost.innerHTML = inputCost.value

  btnEdit.innerHTML = 'edit'
  btnDelete.innerHTML = 'delete'

  divElListExpItem.appendChild(textCost)
  divElListExpItem.appendChild(textToe)
  
  divtextToPayment.appendChild(textToPayment)

  div2btns.appendChild(btnEdit)
  div2btns.appendChild(btnDelete)

  li.appendChild(divElListExpItem)
  li.appendChild(divtextToPayment)
  li.appendChild(div2btns)

  ul.appendChild(li)

  divExpList.appendChild(ul)

  

  // IL METODO "substring" mi aiuta a nascondere dei caratteri che sono visti come array a base 0
  // esempio: £ 500 --> substring(2) = 500 (contare anche gli spazi)

  let expenseValue = inputCost.value.substring(2)

  totalExpenses.push(+expenseValue)

  sommaDaSottrarre = totalExpenses.reduce((a, b) => {
    return a + b
  })


  initialBudget.innerHTML = initialBudget.innerHTML.substring(2) - sommaDaSottrarre

  console.log('CIAO FRATE', sommaDaSottrarre)

  window2.style.display = 'none'
  window1.style.display = 'block'

  inputToe.value = ''
  inputCost.value = ''

  bigEditBtn.style.display = 'none'
  btnConfirm.style.display = 'block'

  console.log(initialBudget.innerHTML)

  // IN QUESTO UTILIZZO L'addEventListener COME UNA CALLBACK FUNCTION.
  // IL PRIMO PARAMENTRO È UN EVENTO (CLICK) IL SECONDO È UNA FUNZIONE ANONIMA (NON HA NOME)

  btnEdit.addEventListener('click', (e) => {

    labelNewExpense.innerHTML = 'Edit expense'

    bigEditBtn.style.display = 'block'
    btnConfirm.style.display = 'none'

    window2.style.display = 'block'
    window1.style.display = 'none'

    console.log(e.target) // a questo evento prendimi questo target 

    thisVeryListItem = e.target.parentNode.parentNode // ora la "thisverylistitem" è uguale al "btnedit"

    let newtextToe = e.target.parentNode.parentNode.children[0].children[1]
    let newtextCost = e.target.parentNode.parentNode.children[0].children[0]

    inputToe.value = newtextToe.innerHTML
    inputCost.value = newtextCost.innerHTML

    console.log('millo mi', thisVeryListItem)
  })

  btnDelete.addEventListener('click', (e) => {

    ul.removeChild(li)

    console.log(btnDelete)

  })

}

btnConfirm.addEventListener('click', function () {
 
  addElemInUl()

})

// QUESTA FUNZIONE MODIFICHERÀ UNA SPESA GIA ESISTENTE // 

let bigEdit = () => {

  labelNewExpense.innerHTML = 'New expense'

  bigEditBtn.style.display = 'none'
  btnConfirm.style.display = 'block'

  window2.style.display = 'none'
  window1.style.display = 'block'

  let newtextToe = thisVeryListItem.children[0].children[1]
  let newtextCost = thisVeryListItem.children[0].children[0]

  newtextToe.innerHTML = inputToe.value
  newtextCost.innerHTML = inputCost.value

  console.log()

}

bigEditBtn.addEventListener('click', function () {

  bigEdit()

})

// CON QUESTE FUNZIONI SELEZIONERÒ IL TIPO DI PAGAMENTO //

let chooseTypePay1 = ()=> {

  // IL PUNTO ESCLAMATIVO DAVANTI A UNA VARIABILE RENDE LA STESSA OPPOSTA AL SUO VALORE O LA NEGA (TRUE => FALSE)

btnCashIsSelected = !btnCashIsSelected
btnCash.classList.toggle('topSelected')

if (btnCashIsSelected === true) {
    btnDebitIsSelected = false 
    btnCreditIsSelected = false
    btnDebit.className = 'btn2'
    btnCredit.className = 'btn3'

} else {
  btnCashIsSelected = false
}
console.log('btnCashIsSelected', btnCashIsSelected)
}

btnCash.addEventListener('click', () => {

  chooseTypePay1()
})

////////////////////////////////////////////////////////////////////

let chooseTypePay2 = () => {

  btnDebitIsSelected = !btnDebitIsSelected
  btnDebit.classList.toggle('topSelected')

  if (btnDebitIsSelected === true){
    btnCashIsSelected = false
    btnCreditIsSelected = false
    btnCash.className = 'btn1'
    btnCredit.className = 'btn3'
  }
  else {
    btnDebitIsSelected = false
  }
  console.log('btnDebitIsSelected', btnDebitIsSelected)
}

btnDebit.addEventListener('click', () => {

  chooseTypePay2()
})

/////////////////////////////////////////////////////////////////////

let chooseTypePay3 = () => {

  btnCreditIsSelected = !btnCreditIsSelected
  btnCredit.classList.toggle('topSelected')

  if (btnCreditIsSelected === true){  
    btnCashIsSelected = false
    btnDebitIsSelected = false
    btnDebit.className = 'btn2'
    btnCash.className = 'btn1'
} 
else {
  btnCreditIsSelected = false
}


  console.log('btnCreditIsSelected', btnCreditIsSelected)

}

btnCredit.addEventListener('click', ()=>{

  chooseTypePay3()
})



